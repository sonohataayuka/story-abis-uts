from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from django.apps import apps
from django.http import HttpRequest
from .views import logIn, signUp, logOut
from .apps import Story9Config
from .forms import SignUpForm, LoginForm
# Create your tests here.

class Story9Test(TestCase):
    #LOGIN PAGE TEST
    def test_does_login_page_exist(self):
        response = Client().get(reverse("login"))
        self.assertEqual(response.status_code, 200)
    
    def test_check_function_used_by_login(self):
        found = resolve('/login/')
        self.assertEqual(found.func, logIn)

    def test_does_login_views_show_correct_template(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_check_login_page_have_form(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<form', html_response)
    
    def test_signin_header(self):
        request = HttpRequest()
        response = logIn(request)
        html_response = response.content.decode('utf8')
        self.assertContains(response, "Sign in")

    #SIGN UP PAGE TEST
    def test_does_login_page_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)
    
    def test_check_function_used_by_login(self):
        found = resolve('/signup/')
        self.assertEqual(found.func, signUp)
    
    def test_signup_header(self):
        response = Client().get('/signup/')
        self.assertContains(response, "Create a new account")

    def test_does_signup_views_show_correct_template(self):
        response = Client().get('/signup/')
        self.assertTemplateUsed(response, 'signup.html')
    
    #SIGN OUT TEST
    def test_does_sign_out_work(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    #FORM TEST
    def test_login_form_invalid(self):
        login_form = LoginForm(data={})
        self.assertFalse(login_form.is_valid())
        signup_form = SignUpForm(data={})
        self.assertFalse(signup_form.is_valid())
    
class LogInTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)
    def test_login(self):
        # send login data
        response = self.client.post('/login/', self.credentials, follow=True)
        # should be logged in now
        self.assertTrue(response.context['user'].is_active)

class TestApp(TestCase):
	def test_app(self):
		self.assertEqual(Story9Config.name, "story9")