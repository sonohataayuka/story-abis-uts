from django.test import TestCase

# Create your tests here.

from django.test import TestCase
from django.apps import apps
from django.test import Client
from django.urls import resolve, reverse
from .views import index
from story7.apps import Story7Config

# Create your tests here.
class UrlsTest(TestCase):

    def setUp(self):
        self.index = reverse("story7:index")

    def test_index_use_right_function(self):
        found = resolve(self.index)
        self.assertEqual(found.func, index)


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.index = reverse("story7:index")

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')


class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('story7').name, 'story7')